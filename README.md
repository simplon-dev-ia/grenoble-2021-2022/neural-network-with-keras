# Neural network and GAN

Utilisation des réseaux de neurones avec la bibliothèque Keras :

- Classification et régression avec des données tabulaires
- Analyse de l'impact des hyperparamètres
- Modèles pour l'image
- Modèles génératifs d'images (transfert de style)