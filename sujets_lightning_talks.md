# Sujets de lightning talks

Concepts :
- Fonction d'activation ELU et SELU
- Courbe de Lorenz et coefficient de Gini -> Gabriel
- Courbe ROC et AUC -> Armand
- Métriques de régression : MAE, MSE, RMSE, R2 -> Merouane

Outils :
- Figma -> Rayane
- Dataiku -> Nolan

Histoire :
- Le turc mécanique -> Marouan
- Ada Lovelace
- Test de Turing
- ELIZA -> Cinthya
- Le perceptron
- Deep Blue
- Alpha Go -> Thienvu